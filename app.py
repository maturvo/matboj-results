from flask import Flask, render_template, request
from flask import json
app = Flask(__name__)


import gspread
from oauth2client.service_account import ServiceAccountCredentials

import datetime

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('Matboj-99250cded819.json', scope)
client = gspread.authorize(creds)

shname = 'Score {} - sorted'
sheetname = '2019 BB - systém na body'

sheet = client.open(sheetname)

categories = {"T": "Tercia", "K": "Kvarta", "8": "8. ročník", "9": "9. ročník",
              "5": "5. ročník", "6": "6.ročník", "7": "7. ročník", "P": "Prima", "S": "Sekunda"}
columns = ["Poradie", "Názov tímu", "Škola", "Body"]

loaded = {}

end = datetime.datetime(2019, 11, 29, 12, 00)
freeze = datetime.timedelta(minutes=15)

def frozen():
    return datetime.datetime.now() + freeze > end

@app.route('/<string:cats>')
def live_results(cats):
    return render_template('matboj-vysledky.html', categories=cats, columns=columns)


@app.route('/results/<cat>')
def results(cat):
    if not frozen():
        print(loaded)
        try:
            loaded[cat] = sheet.worksheet(shname.format(cat)).get_all_records()
        except:
            print("rip")
        print(loaded)
    data = {"results": loaded[cat], "category": categories[cat], "frozen": frozen()}
    return app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )

@app.route('/final/<cat>')
def final(cat):
    data = {"results": sheet.worksheet(shname.format(cat)).get_all_records(), "category": categories[cat], "frozen": frozen()}	
    return render_template('final.html', vysledky=json.dumps(data), columns=columns, max_teams=request.args.get("max_teams", 5))


@app.route('/tombola')
def tombola():
    listky = sheet.worksheet("Raffle").get_all_records()
    timy = []
    nazvy = []
    skoly = []
    for t in listky:
        timy += [t['ID']]*t['Tombolových lístkov']
        nazvy += [t['Názov tímu']]*t['Tombolových lístkov']
        skoly += [t['Škola']]*t['Tombolových lístkov']
    return render_template('tombola.html', timy=timy, nazvy=nazvy, skoly=skoly)


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
